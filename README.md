# anagrams-with-friends

Web app implementation of
[Letter Jam](https://czechgames.com/en/letter-jam/) by Ondra Skoupý,
published by Czech Games Edition.

Also can be used to take notes while playing the physical game.
